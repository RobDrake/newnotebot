var irc = require('irc-connect');
var db = require('./dbmodule.js');
var config = require('./config/botconfig');
var sleep = require('msleep');
var ircOptions = {
    port : 6667,
    secure : false,
    nick : config.irc.botName,
    realname : 'robdrake',
    ident : 'robdrake'
};

var freenode = irc.connect(config.irc.server, ircOptions)
    .use(irc.pong, irc.names, irc.motd)
    .on('welcome', function(msg) {
        this.send('PRIVMSG NickServ :IDENTIFY ', config.irc.botName, ' ', config.irc.nickPass);
    })
    .on('identified', function(nick) {
        console.log('Joining the asylum');
        this.send('JOIN ' + config.irc.channels);
    })

    .on('nick', function(nick) {
        console.log(nick);
    } )
    .on('NOTICE', function(event) {
        console.log('NOTICE; ' + event.params[1]);
    })
    .on('PRIVMSG', function(event) {
        var params = event.params;
        var channel = params[0];
        var words = params[1].split(' ');
        var command = words.shift();
        words = words.join(' ');

        console.log(channel +  ':' + command + ':' + words);


        if(command[0] !== '&') {    //Not a bot command
            return;
        }

        if(channel === config.irc.botName) {
            if(event.nick === 'robdrake') {
                if(command === '&quit') {
                    console.log('quitting boss');
                    this.send('QUIT :Yes boss!');
                    process.exit(0);
                }
            }
        }

        if(channel === config.irc.botName) {
            channel = event.nick;
        }

        db.botCommand(event.nick, event.host, channel, command, words, botSay, this);
   });

    function botSay(who, text, that) {

        if(!Array.isArray(text)) {
            that.send('PRIVMSG ' + who + ' :' + text);
        }
        else {
            text.map( function(speech) {
                that.send('PRIVMSG ' + who + ' :' + speech);
                sleep(3000);
            });
        }
    }


var	promise = require('bluebird');
var 	options = {
	promiseLib: promise
};
var pgp = require('pg-promise')(options);
var config = require('./config/dbaseconfig');

var 	cn = {
	host : 'localhost',
	port : 5432,
	database : config.database.database,
	user : config.database.user,
	password : config.database.password
};

var db = pgp(cn);

function Quit() {
	pgp.end();
	process.exit();
}

var botCommand = function(nick, host, chan, command, words, outputFunction, that) {
	var out = [];

	if( chan === '') {
		chan = nick;
	}

	switch(command) {
		case '&help':
			out = [];
			out.push('!number   : lists the number of notes ' + nick + ' has.');
			out.push('!add      : Adds a note with ' + nick + ' as the author.');
			out.push('!listall  : Displays all notes ' + nick + ' has in the database.');
			out.push('!list     : Lists a notes id and a brief beginning of the note');
			out.push('!del      : Deletes a single note based on noteid from !list only authored by ' + nick + '.');
			out.push('!read     : Reads a single note based on noteid authored by ' + nick +  '.');
			outputFunction(chan, out, that);
			break;
		case '&number':
			db.manyOrNone('SELECT doc from notetable where author = $1 AND host = $2', [nick, host])
			.then( function( data) {
				if( data.length === 0) {
					outputFunction(chan, "Sorry " + nick + ", no notes from you", that);
				} else {
					outputFunction( chan, nick + " you have " + data.length + " notes.", that);
				}
			})
			.catch( function( error) {
				console.log( "Error: " + error);
				Quit();
			});
			break;
		case '&add':
			var data = "";

			data = words.slice(0).join(' ');
			if( data.length > 255) {
				data = data.slice(0, 255);
			}

			db.none('INSERT INTO notetable(author, doc, host) VALUES($1, $2, $3)', [nick, data, host])
			.then( function() {
				outputFunction(chan, nick + " note added.", that);
			})
			.catch( function(error) {
				console.log("Error: " + error);
				Quit();
			});
			break;
		case '&listall':
			db.manyOrNone('SELECT doc FROM notetable WHERE author = $1 AND host = $2', [nick, host], that)
			.then( function(data) {
				if(data.length === 0) {
					outputFunction( chan, "Sorry " + nick + ", no notes for you", that);
				} else {
					out = [];
					data.forEach( function( row ) {
						out.push(row.doc);
					});
					outputFunction( chan, out, that);
				}
			})
			.catch( function(error) {
				console.log("Error: " + error);
				Quit();
			});
			break;
		case '&del':
			db.manyOrNone('SELECT * FROM notetable WHERE noteid = $1 AND author = $2 AND host = $3', [words[0], nick, host])
			.then( function(data) {
				if( data.length === 0) {
					out = [];
					out.push("Sorry " + nick + ", That note wasn't found.");
					out.push("Try !list");
					outputFunction(chan, out, that);
				} else {
					db.none('DELETE FROM notetable WHERE noteid=$1 AND author=$2 AND host=$3', [words[0], nick, host])
					.then( function(data) {
						outputFunction( chan, "Record " + words[0] + " deleted", that);
					});
				}
			})
			.catch( function(error) {
				console.log("Error: " + error);
				Quit();
			});
			break;
		case '&list':
			db.manyOrNone('SELECT noteid, doc FROM notetable WHERE author = $1 AND host = $2', [nick, host])
			.then( function(data) {
				if(data.length === 0) {
					outputFunction(chan, "Sorry " + nick + ", no notes from you", that);
				} else {
					out = [];
					data.forEach( function( row ) {
						out.push( row.noteid + ":" + row.doc.substring(0, 15) );
					});
					outputFunction( chan, out, that );
				}
			})
			.catch( function(error) {
				console.log("Error: " + error);
				Quit();
			});
			break;
		case '&read':
			console.log(words[1]);
			db.manyOrNone('SELECT doc FROM notetable WHERE author = $1 AND noteid = $2 AND host = $3', [nick, words[0], host])
			.then( function(data) {
				if( data.length === 0) {
					outputFunction(chan, "Note not found!", that);
				} else {
					outputFunction(chan, data[0].doc, that);
				}
			})
			.catch( function(error) {
				console.log("Error: " + error);
				Quit();
			});

			break;
		default:
			// '&' is a common bot command identifier, if the command is not
			// list, ignore it.
			break;
	}
};

module.exports.botCommand = botCommand;
